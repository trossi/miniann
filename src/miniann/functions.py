import numpy as np


class LinearFunction:
    @staticmethod
    def __call__(x):
        return np.asarray(x)

    @staticmethod
    def derivative(x):
        return np.ones_like(x)


class ReLUFunction:
    @staticmethod
    def __call__(x):
        return x * (x > 0)

    @staticmethod
    def derivative(x):
        return 1.0 * (x > 0)


class SigmoidFunction:
    @staticmethod
    def __call__(x):
        return 1.0 / (1 + np.exp(-x))

    @staticmethod
    def derivative(x):
        y = SigmoidFunction.__call__(x)
        return y * (1 - y)


class SquareFunction:
    @staticmethod
    def __call__(x):
        return x**2

    @staticmethod
    def derivative(x):
        return 2.0 * x


linear = LinearFunction()
relu = ReLUFunction()
sigmoid = SigmoidFunction()
square = SquareFunction()
