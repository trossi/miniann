from functools import partial
import numpy as np


# Note on array shapes / indexing:
# t: independent data entries
# j: inputs to the layer / inputs to each node in the layer
# k: outputs of the layer / enumeration of the nodes in the layer
#
# For example,
# weight_jk is a matrix of shape (j, k), where
# j enumerates the inputs to each node and
# k enumerates the nodes

class Layer:
    def __init__(self, weights, biases, function):
        self.weight_jk = weights
        self.bias_k = biases
        self.func = function
        self.input_tj = None
        self.derivative_tk = None
        self.delta_tk = None

    def evaluate(self, input_tj):
        return self.func(input_tj @ self.weight_jk + self.bias_k)

    def forward(self, input_tj):
        self.input_tj = input_tj
        z_tk = input_tj @ self.weight_jk + self.bias_k
        a_tk = self.func(z_tk)
        self.derivative_tk = self.func.derivative(z_tk)
        return a_tk

    def backward(self, gradient_tk):
        assert gradient_tk.shape == self.derivative_tk.shape
        self.delta_tk = self.derivative_tk * gradient_tk
        return self.delta_tk @ self.weight_jk.T

    def adjust(self, learning_rate, reduction):
        delta_bias_k = reduction(self.delta_tk)
        self.bias_k += learning_rate * delta_bias_k
        delta_weight_jk = reduction(self.delta_tk[:, np.newaxis, :]
                                    * self.input_tj[:, :, np.newaxis])
        self.weight_jk += learning_rate * delta_weight_jk
        self.input_tj = None
        self.derivative_tk = None
        self.delta_tk = None


class ArtificialNeuralNetwork:
    def __init__(self, layers, loss_function):
        assert layers[0].weight_jk.shape[0] == 1, \
            'First layer should have only one input'
        assert layers[-1].weight_jk.shape[1] == 1, \
            'Last layer should have only one output'
        self.layers = layers
        self.loss_func = loss_function

    def evaluate(self, x_t):
        a_tj = x_t[:, np.newaxis]
        for layer in self.layers:
            a_tj = layer.evaluate(a_tj)
        y_t = a_tj[:, 0]
        return y_t

    def ioptimize(self, x_t, y_t, *,
                  epochs=100, learning_rate=0.1, reduction=None):
        if reduction is None:
            reduction = partial(np.average, axis=0)

        for i in range(epochs):
            a_tj = x_t[:, np.newaxis]
            for layer in self.layers:
                a_tj = layer.forward(a_tj)
            ypred_t = a_tj[:, 0]
            err = np.average(self.loss_func(y_t - ypred_t))
            yield err
            gradient_t = self.loss_func.derivative(y_t - ypred_t)
            gradient_tk = gradient_t[:, np.newaxis]
            for layer in self.layers[::-1]:
                gradient_tk = layer.backward(gradient_tk)
            for layer in self.layers:
                layer.adjust(learning_rate, reduction)

    def optimize(self, *args, **kwargs):
        for i, err in enumerate(self.ioptimize(*args, **kwargs)):
            print('\r', end='')
            print(f'{i:8} err={err:.8f}', end=' ')
        print()
