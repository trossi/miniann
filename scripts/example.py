import numpy as np
import matplotlib.pyplot as plt

from miniann.functions import linear, sigmoid, square
from miniann.network import Layer, ArtificialNeuralNetwork

rng = np.random.default_rng(seed=42)

# Training data
Nt = 101
x_t = np.linspace(0, 10, Nt)
y_t = np.sin(x_t) + 0.3 * rng.standard_normal(Nt)

# Build model
layers = [
    Layer(rng.standard_normal((1, 1)), np.zeros(1),
          function=linear),
    Layer(rng.standard_normal((1, 32)), -np.linspace(0, 10, 32),
          function=sigmoid),
    Layer(rng.standard_normal((32, 1)), np.zeros(1),
          function=linear),
]
ann = ArtificialNeuralNetwork(layers, loss_function=square)

# Optimize
epochs = 2000
for i, err in enumerate(ann.ioptimize(x_t, y_t, epochs=epochs,
                                      learning_rate=0.1)):
    print('\r', end='')
    print(f'{i:8} err={err:.8f}', end='')
print()

# Evaluate
ypred_t = ann.evaluate(x_t)

# Plot
plt.figure()
plt.scatter(x_t, y_t, color='b')
plt.plot(x_t, ypred_t, 'r')
plt.show()
