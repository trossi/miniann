import numpy as np
import matplotlib.pyplot as plt

from miniann.functions import linear, relu, sigmoid, square
from miniann.network import Layer, ArtificialNeuralNetwork

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Disable tensorflow logging
import tensorflow as tf  # noqa

tf.random.set_seed(42)
rng = np.random.default_rng(seed=42)

# Training data
Nt = 101
x_t = np.linspace(0, 10, Nt)
y_t = np.sin(x_t) + 0.3 * rng.standard_normal(Nt)

# Build model
model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(units=1, activation='linear', input_shape=[1]))
model.add(tf.keras.layers.Dense(units=32, activation='sigmoid'))
model.add(tf.keras.layers.Dense(units=1, activation='linear'))
model.compile(loss='mse',
              optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.01))
model.summary()

# Optimize
epochs = 2000


class Log(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        print(f"\r{epoch:5}/{epochs:5} loss={logs['loss']:.8f}", end='')

    def on_train_end(self, logs={}):
        print()


model.fit(x_t, y_t, epochs=epochs, verbose=0, callbacks=[Log()])

# Evaluate
ypred_t = model.predict(x_t, verbose=0)

# Build and evaluate corresponding model using miniann
func_name = {'linear': linear, 'relu': relu, 'sigmoid': sigmoid}
layers = []
for layer in model.layers:
    func = func_name[layer.activation.__name__]
    weights = layer.get_weights()
    layers.append(Layer(weights[0], weights[1], func))

ann = ArtificialNeuralNetwork(layers, loss_function=square)
ypred2_t = ann.evaluate(x_t)

# Plot
plt.figure()
plt.scatter(x_t, y_t, color='b')
plt.plot(x_t, ypred_t, 'r')
plt.plot(x_t, ypred2_t, 'b:')
plt.show()
