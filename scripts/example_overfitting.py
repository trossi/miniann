import numpy as np
import matplotlib.pyplot as plt

from miniann.functions import linear, sigmoid, square
from miniann.network import Layer, ArtificialNeuralNetwork

rng = np.random.default_rng(seed=42)

# Training data
Nt = 21
x_t = np.linspace(0, 10, Nt)
y_t = np.sin(x_t) + 0.3 * rng.standard_normal(Nt)

# Validation data
Nv = 20
x_v = rng.uniform(0, 10, Nv)
y_v = np.sin(x_v) + 0.3 * rng.standard_normal(Nv)

# Build model
layers = [
    Layer(rng.standard_normal((1, 1)), np.zeros(1),
          function=linear),
    Layer(rng.standard_normal((1, 32)), -np.linspace(0, 10, 32),
          function=sigmoid),
    Layer(rng.standard_normal((32, 1)), np.zeros(1),
          function=linear),
]
ann = ArtificialNeuralNetwork(layers, loss_function=square)

# Optimize
epochs = 40000
error_i = []
validation_error_i = []
for i, err in enumerate(ann.ioptimize(x_t, y_t, epochs=epochs,
                                      learning_rate=0.02)):
    # Check validation error too
    ypred_v = ann.evaluate(x_v)
    verr = np.average(square(y_v - ypred_v))
    print('\r', end='')
    print(f'{i:8} err={err:.8f} verr={verr:.8f}', end=' ')
    error_i.append(err)
    validation_error_i.append(verr)
print()

# Evaluate
ypred_t = ann.evaluate(x_t)

# Plot
fig, axs = plt.subplots(1, 2, figsize=(16, 6))
ax = axs[0]
ax.scatter(x_t, y_t, color='b', label='training data')
ax.scatter(x_v, y_v, color='c', label='validation data')
ax.plot(x_t, ypred_t, 'r', label='model')
ax.set_xlabel('x')
ax.set_xlabel('y')
ax.legend()

ax = axs[1]
epoch_i = list(range(1, epochs + 1))
ax.plot(epoch_i, error_i, 'b', label='training error')
ax.plot(epoch_i, validation_error_i, 'r', label='validation error')
ax.set_yscale('log')
ax.set_xlabel('epoch')
ax.set_ylabel('error')
ax.legend()

plt.show()
