import numpy as np
import matplotlib.pyplot as plt
import torch

from miniann.functions import linear, relu, sigmoid, square
from miniann.network import Layer, ArtificialNeuralNetwork

torch.manual_seed(42)
rng = np.random.default_rng(seed=42)

# Training data
Nt = 101
x_t = np.linspace(0, 10, Nt)
y_t = np.sin(x_t) + 0.3 * rng.standard_normal(Nt)

# Convert to torch tensors
torch.set_default_dtype(torch.float64)
x_tj = torch.from_numpy(x_t[:, np.newaxis])
y_tj = torch.from_numpy(y_t[:, np.newaxis])

# Build model
model = torch.nn.Sequential(
    torch.nn.Linear(1, 1),
    torch.nn.Linear(1, 32),
    torch.nn.Sigmoid(),
    torch.nn.Linear(32, 1),
)
loss_fn = torch.nn.MSELoss(reduction='mean')

# Optimize
learning_rate = 0.05
optimizer = torch.optim.RMSprop(model.parameters(), lr=learning_rate)
for i in range(2000):
    ypred_t = model(x_tj)

    loss = loss_fn(ypred_t, y_tj)
    err = loss.item()
    print('\r', end='')
    print(f'{i:8} err={err:.8f}', end='')

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
print()

# Evaluate
ypred_t = model(x_tj).detach().numpy()

# Build and evaluate corresponding model using miniann
func_name = {'linear': linear, 'relu': relu, 'sigmoid': sigmoid}
layers = []
for layer in model:
    func_str = str(layer).split('(')[0].lower()
    func = func_name[func_str]
    if func_str == 'linear':
        weights, biases = layer.parameters()
        layers.append(Layer(weights.detach().numpy().T,
                            biases.detach().numpy(),
                            func))
    else:
        # Change the activation function of the previous layer
        layers[-1].func = func

ann = ArtificialNeuralNetwork(layers, loss_function=square)
ypred2_t = ann.evaluate(x_t)

# Plot
plt.figure()
plt.scatter(x_t, y_t, color='b')
plt.plot(x_t, ypred_t, 'r')
plt.plot(x_t, ypred2_t, 'b:')
plt.show()
