# miniann - minimal demo artificial neural network

Miniann is a minimal implementation of an artificial neural network
for educational purposes.

## Set up virtual environment

```
python3 -m virtualenv venv
source venv/bin/activate
pip install -e .
```

## Examples

### Fitting data

```
python scripts/example.py
```

### Comparison to pytorch

This requires `pip install torch==1.12.0` (other versions likely compatible too).

```
python scripts/comparison_pytorch.py
```

### Comparison to tensorflow

This requires `pip install tensorflow==2.9.1` (other versions likely compatible too).

```
python scripts/comparison_tensorflow.py
```

### Overfitting case

```
python scripts/example_overfitting.py
```

