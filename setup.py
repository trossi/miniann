from setuptools import setup, find_packages

setup(name="miniann",
      version="0.1",
      description="Artificial Neural Network - Minimal Demo",
      license="GPLv3+",
      packages=find_packages("src"),
      package_dir={"": "src"},
      install_requires=[
          "numpy>=1.7.0",
          "matplotlib>=3.1.0",
          ],
      )
